#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from E7446 device
$(call inherit-product, device/micromax/E7446/device.mk)

PRODUCT_DEVICE := E7446
PRODUCT_NAME := lineage_E7446
PRODUCT_BRAND := Micromax
PRODUCT_MODEL := E7446
PRODUCT_MANUFACTURER := micromax

PRODUCT_GMS_CLIENTID_BASE := android-micromax

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="E7446-user 11 RP1A.200720.011 1637827892 release-keys"

BUILD_FINGERPRINT := Micromax/E7446/E7446:11/RP1A.200720.011/1637827892:user/release-keys
